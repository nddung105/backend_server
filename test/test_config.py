from src.utils.config import CONFIG

print("---1", CONFIG)

def select(table, fields):
    sub = ["%s"] * len(fields)
    print(sub)
    query = f"""
        INSERT INTO {table} ({', '.join(fields)}) VALUES ({", ".join(sub)})
    """
    print(query)

select('user',['id', 'name'])
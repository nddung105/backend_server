from datetime import datetime
import psutil, requests
import time

while True:
    time.sleep(2)
    data = {
        "cpu": psutil.cpu_percent(),
        "ram": psutil.virtual_memory().percent,
        "time": datetime.now().strftime("%H:%M:%S"),
        "storage": 50,
        "id": 1}
    print(data)
    requests.post("http://localhost:8000/logs/info", json=data)
from src.db.database import db


def select(table, fields=[], condition=''):
    query = f"""
        SELECT {'*' if len(fields) < 1 else ', '.join(fields)} FROM {table} {'WHERE ' + condition if condition else ''}

    """
    print(query)
    cursor = db.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    return result


def insert(table, fields, values):
    sub = ["%s"] * len(fields)
    query = f"""
        INSERT INTO {table} ({', '.join(fields)}) VALUES ({", ".join(sub)})
    """
    cursor = db.cursor()
    cursor.execute(query, values)
    db.commit()
    return

def delete(table, condition):
    query = f"""
        DELETE FROM {table} WHERE {condition}
    """
    cursor = db.cursor()
    cursor.execute(query)
    db.commit()
    return

def create_table(query):
    cursor = db.cursor()
    cursor.execute(query)
    return

def insert_obj(table, obj):
    obj_dict = obj.__dict__
    fields, values = list(obj_dict.keys()), list(obj_dict.values())
    sub = ["%s"] * len(fields)
    query = f"""
        INSERT INTO {table} ({', '.join(fields)}) VALUES ({", ".join(sub)})
    """
    cursor = db.cursor()
    cursor.execute(query, values)
    db.commit()
    return
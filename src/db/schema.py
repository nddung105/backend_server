from src.db.crud import create_table

aibox = """
    CREATE TABLE aibox (
    id INT NOT NULL AUTO_INCREMENT,
    ip VARCHAR(100) NOT NULL,
    name VARCHAR(100) NOT NULL,
    time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    UNIQUE INDEX id_UNIQUE (id ASC));
"""

camera = """
    CREATE TABLE camera (
    id INT NOT NULL AUTO_INCREMENT,
    id_aibox INT NOT NULL,
    id_local INT NOT NULL,
    rtsp VARCHAR(100) NOT NULL,
    name VARCHAR(100) NOT NULL,
    time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (id_aibox) REFERENCES aibox(id),
    UNIQUE INDEX id_UNIQUE (id ASC));
"""

log = """
    CREATE TABLE log (
    id INT NOT NULL AUTO_INCREMENT,
    content VARCHAR(100) NOT NULL,
    id_camera INT NOT NULL,
    id_aibox INT NOT NULL,
    log_path VARCHAR(100) NOT NULL,
    time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (id_aibox) REFERENCES aibox(id),
    UNIQUE INDEX id_UNIQUE (id ASC));
"""

mask = """
    CREATE TABLE mask (
    id INT NOT NULL AUTO_INCREMENT,
    mask_status INT NOT NULL,
    id_camera INT NOT NULL,
    score FLOAT NOT NULL,
    id_aibox INT NOT NULL,
    log_path VARCHAR(100) NOT NULL,
    time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (id_aibox) REFERENCES aibox(id),
    UNIQUE INDEX id_UNIQUE (id ASC));
"""

info = """
    CREATE TABLE info (
    id INT NOT NULL AUTO_INCREMENT,
    cpu FLOAT NOT NULL,
    ram FLOAT NOT NULL,
    storage INT NOT NULL,
    id_aibox INT NOT NULL,
    time DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (id_aibox) REFERENCES aibox(id),
    UNIQUE INDEX id_UNIQUE (id ASC));
"""

create_table(aibox)
create_table(camera)
create_table(log)
create_table(info)
create_table(mask)

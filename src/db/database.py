from code import interact
import mysql.connector
from src.utils.config import CONFIG
import time

mysql_config = CONFIG['mysql']

db = mysql.connector.connect(
      host = mysql_config['host'],
      user = mysql_config['user'],
      passwd = mysql_config['password'],
      database = mysql_config['database']
)
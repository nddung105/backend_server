from typing import List, Optional

from datetime import datetime

from pydantic import BaseModel


class AIBOXInsert(BaseModel):
    ip: str
    name: str

class Info(BaseModel):
    cpu: float
    ram: float
    storage: float
    id_aibox: int

class CameraAdd(BaseModel):
    rtsp: str
    name: str
    id_aibox: Optional[int] = None
    id_local: str

class LogAdd(BaseModel):
    content: str
    log_path: str
    id_camera: Optional[int] = None
    id_aibox: Optional[int] = None
    
class LogMask(BaseModel):
    log_path: str
    mask_status: Optional[int] = 0
    score: Optional[float] = 0
    id_camera: Optional[int] = None
    id_aibox: Optional[int] = None
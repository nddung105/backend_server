from typing import List
from urllib import request
from src.db.model import LogAdd, Info, LogMask
from src.utils.config import CONFIG
import os
from src.db.crud import insert_obj, select

def add(log: LogAdd):
    try:
        insert_obj(table='log', obj=log)
        return True
    except Exception as e:
        print(e)
        return False

def add_info(info: Info):
    try:
        insert_obj(table='info', obj=info)
        return True
    except Exception as e:
        print(e)
        return False

def add_log_mask(log: LogMask):
    try:
        insert_obj(table='mask', obj=log)
        return True
    except Exception as e:
        print(e)
        return False
    
def get():
    try:
        return select(table='log')
    except Exception as e:
        print(e)
        return False

def check_folder(path):
    _ = path.split('/')
    folder = "/".join(_[:-1])
    if not os.path.isdir(folder):
        os.mkdir(folder)



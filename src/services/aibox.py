from typing import List
from src.db.model import AIBOXInsert, CameraAdd
from src.utils.config import CONFIG
from src.db.crud import insert_obj, select, delete
import os

def add_camera(camera: CameraAdd):
    try:
        insert_obj(table='camera', obj=camera)
        return
    except Exception as e:
        print(e)
        return False


def add(data: AIBOXInsert):
    try:
        insert_obj(table='aibox', obj=data)
        return 
    except Exception as e:
        print(e)
        return False


def get():
    try:
        return select(table='aibox')
    except Exception as e:
        print(e)
        return False


def delete_aibox(id):
    try:
        delete(table='aibox', condition= f"id={id}")
        delete(table='camera', condition= f"id_aibox={id}")
    except Exception as e:
        print(e)
        return False


def get_camera():
    try:
        return select(table='camera')
    except Exception as e:
        print(e)
        return False


def get_camera_by_id_local(id_local, id_aibox):
    try:
        return select(table='camera', condition=f'id_local={id_local} AND id_aibox={id_aibox}')
    except Exception as e:
        print(e)
        return False


def get_by_id(id):
    try:
        return select(table='aibox', condition=f'id="{id}"')
    except Exception as e:
        print(e)
        return False

from imp import reload
from pydantic import BaseModel
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from src.routers import logs
from src.routers import aibox
from src.db.database import db
import socketio
import json
import uvicorn
from starlette.responses import FileResponse 
from fastapi.staticfiles import StaticFiles

sio = socketio.AsyncServer(
	async_mode="asgi", cors_allowed_origins="*"
)
asgi = socketio.ASGIApp(sio)

app = FastAPI()

app.add_middleware(
	CORSMiddleware,
	allow_origins=["*"],
	allow_credentials=True,
	allow_methods=["*"],
	allow_headers=["*"],
)

app.include_router(logs.router)
app.include_router(aibox.router)


class Stream(BaseModel):
    img: str
    id: int


@app.post("/stream")
async def stream_camera(data: Stream):
    try:
        await sio.emit("stream", json.dumps({
            "img": data.img,
            "id": data.id
        }))
        return {"message": "Success"}
    except Exception as e:
        return {"message": e}

app.mount("/ws", asgi)

@app.get('/')
def get():
    return FileResponse("src/templates/cam1.html")

@app.get('/video1')
def get():
    return FileResponse("src/templates/video1.html")

@app.get('/video2')
def get():
    return FileResponse("src/templates/video2.html")

@app.get('/video3')
def get():
    return FileResponse("src/templates/video3.html")

@app.get('/video4')
def get():
    return FileResponse("src/templates/video4.html")

@app.get('/cam1')
def get():
    return FileResponse("src/templates/cam1.html")

@app.get('/cam2')
def get():
    return FileResponse("src/templates/cam2.html")

app.mount("/logs", StaticFiles(directory="logs"), name="logs")

@app.on_event("shutdown")
def shutdown_event():
    try:
        db.close()
    except Exception as e:
        print("shutdown_event ===== ",e)

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
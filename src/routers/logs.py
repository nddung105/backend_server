from fastapi import APIRouter, File, UploadFile

from src.db.model import LogAdd, Info, LogMask
from src.services.logs import add, get, add_info, check_folder, add_log_mask

router = APIRouter(prefix='/logs')


@router.post('/add')
def log_add(log: LogAdd):
    res = add(log)
    return res


@router.get('/get')
def log_get():
    res = get()
    return res


@router.post('/info')
def info(info: Info):
    res = add_info(info)
    return res

@router.post('/file_video')
async def file_video(path: str, file: UploadFile = File(...)):
    try:
        content = await file.read()
        check_folder(path)
        with open(path, 'wb') as f:
            f.write(content)
        return 200
    except Exception as e:
        print("logs file_video ", e)
        return 400

@router.post('/file_mask')
async def file_video(path: str, file: UploadFile = File(...)):
    try:
        content = await file.read()
        with open('logs/mask/' + path, 'wb') as f:
            f.write(content)
        return 200
    except Exception as e:
        print("logs file_mask ", e)
        return 400

@router.post('/mask')
async def file_video(logs: LogMask):
    try:
        logs.log_path = 'logs/mask/' + logs.log_path
        res = add_log_mask(logs)
        return 200
    except Exception as e:
        print("logs mask ", e)
        return 400
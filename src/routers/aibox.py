from fastapi import APIRouter
from typing import List
from src.db.model import AIBOXInsert, CameraAdd
from src.services.aibox import add, delete_aibox, get, add_camera, get_by_id, get_camera

router = APIRouter()


@router.post('/add')
def aibox_add(data: AIBOXInsert):
    result = add(data=data)
    return result


@router.get('/get')
def aibox_get():
    result = get()
    return result


@router.get('/get_camera')
def aibox_get_camera():
    result = get_camera()
    return result


@router.get('/get_by_id')
def aibox_get(id: int):
    result = get_by_id(id=id)
    return result


@router.post('/delete')
def aibox_delete(id: int):
    result = delete_aibox(id)
    return result


@router.post('/add_camera')
def aibox_add_camera(camera: CameraAdd):
    result = add_camera(camera=camera)
    return result

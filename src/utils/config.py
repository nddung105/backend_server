import yaml

def read_config():
    with open("src/resources/config.yaml", "r") as stream:
        config = yaml.load(stream, Loader=yaml.FullLoader)
    print(config)
    return config

CONFIG = read_config()
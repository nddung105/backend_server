
----
### Export Python Path

` $ export PYTHONPATH=$PWD`

----
### Config and run create tables

- Config MySQL src/resource/config.yaml
- Create table and schema \
` $ python3 src/db/schema.py`

----
### Run Backend

` $ python3 src/main.py`
#!/bin/bash

user=$USER

path_run_service=/usr/include/AI_DAY_DongThap/
path_service=/etc/systemd/system/

name_file_service=techpro_ai_backend_service.service
name_file_run=run_backend.sh

description=Demo_AI_Day_Dong_Thap_Backend

cp ./example_service.service ./$name_file_service

sed -i 's/username/'$user'/g' $name_file_service
sed -i 's/text_description/'$description'/g' $name_file_service

full_path_file_run=$(echo $path_run_service$name_file_run | sed -e 's/\//\\\//g')
sed -i 's/full_path_file_run/'$full_path_file_run'/g' $name_file_service

if [ ! -d "$path_run_service" ]; then
    sudo mkdir $path_run_service
    echo "create folder $path_run_service"
else
    echo "$path_run_service exits"
fi

if [ ! -f "$path_service$name_file_service" ]; then
    sudo cp ./$name_file_service $path_service
    sudo chmod +x $path_service$name_file_service
    echo "copy file $name_file_service to $path_service"
else
    echo "file $name_file_service exist in $path_service"
fi

if [ ! -f "$path_run_service$name_file_run" ]; then
    sudo cp ./$name_file_run $path_run_service
    sudo chmod +x $path_run_service$name_file_run
    echo "copy file $name_file_run to $path_run_service"
else
    echo "file $name_file_run exist in $path_run_service"
fi

sudo systemctl daemon-reload
sudo systemctl enable $name_file_service
sudo systemctl start $name_file_service
